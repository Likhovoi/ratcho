import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Root } from '@app/components/Root';
import registerServiceWorker from '@app/registerServiceWorker';
import '@app/assets/styles/index.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-datepicker/dist/react-datepicker.css';

ReactDOM.render(
  <Root/>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
