import { createLogger } from 'redux-logger';
import { reducer as formReducer } from 'redux-form';
import createSagaMiddleware from 'redux-saga';
import createReduxWaitForMiddleware from 'redux-wait-for-action';
import { combineReducers, applyMiddleware, createStore } from 'redux';
import { startsWith, endsWith, get } from 'lodash';
import { AnyAction, Store } from 'redux';
import { Store as State } from '@app/types/store';
import { AxiosInstance } from 'axios';
// reducers
import authReducer from '@app/store/auth/reducer';
import userReducer from '@app/store/user/reducer';
import thingsCategoriesReducer from '@app/store/thingsCategories/reducer';
// flows
import authFlow from '@app/store/auth/flow';
import userFlow from '@app/store/user/flow';
import thingsCategoriesFlow from '@app/store/thingsCategories/flow';

const createLoggerMiddleware = () => {
  return createLogger({
    collapsed: true,
    colors: {
      title(action: AnyAction) {
        if (endsWith(get(action, 'type'), 'REJECTED')) {
          return 'red';
        } else if (endsWith(get(action, 'type'), 'RECEIVED')) {
          return 'green';
        } else if (endsWith(get(action, 'type'), 'REQUEST')) {
          return 'orange';
        }
        return 'grey';
      },
    },
    predicate(getState: () => any, action: AnyAction) {
      return !startsWith(action.type, '@@redux-form');
    },
  });
};

const newStore = () => {
  const sagaMiddleware = createSagaMiddleware();
  const loggerMiddleware = createLoggerMiddleware();

  const rootReducer = combineReducers<State>({
    form: formReducer,
    auth: authReducer,
    user: userReducer,
    thingsCategories: thingsCategoriesReducer,
  });

  const middlewares = applyMiddleware(
    sagaMiddleware,
    loggerMiddleware,
    createReduxWaitForMiddleware(),
  );

  const store = createStore<State>(rootReducer, middlewares);
  sagaMiddleware.run(authFlow);
  sagaMiddleware.run(userFlow);
  sagaMiddleware.run(thingsCategoriesFlow);
  return store;
};

const watchStore = (http: AxiosInstance, store: Store<State>) => {
  store.subscribe(() => {
    const state = store.getState();
    if (state.auth.token) {
      http.defaults.headers.Authorization = `Bearer ${state.auth.token}`;
    } else {
      delete http.defaults.headers.Authorization;
    }
  });
};

export default function storeInitializer (http: AxiosInstance) {
  const store = newStore();
  watchStore(http, store);
  return store;
}
