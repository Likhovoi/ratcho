import axios from 'axios';

export default function httpInitializer() {
    return axios.create({
        baseURL: process.env.REACT_APP_API_ROOT_URL,
    });
}
