import * as i18n from 'i18next';
import * as Backend from 'i18next-xhr-backend';
import * as LanguageDetector from 'i18next-browser-languagedetector';
import { reactI18nextModule } from 'react-i18next';
import { AxiosInstance } from 'axios';

export default function (baseUrl: string) {
  const backend: Backend.BackendOptions = {
    loadPath: baseUrl + '/locales/{{lng}}/{{ns}}.json',
    allowMultiLoading: false,
    customHeaders: {
      'Content-Type': 'application/json',
    },
    crossDomain: true,
    withCredentials: false,
  };
  
  i18n
    .use(Backend)
    .use(LanguageDetector)
    .use(reactI18nextModule)
    .init({
      whitelist: ['en'],
      ns: ['common', 'auth'],
      defaultNS: 'common',
      fallbackLng: 'en',
      debug: false,
      interpolation: {
        escapeValue: false, // not needed for react!!
      },
      backend,
      react: {
        wait: true
      }
    });

  return i18n;
}
