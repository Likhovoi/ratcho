import i18nInitializer from '@app/services/i18n';
import storeInitializer from '@app/services/store';
import httpInitializer from '@app/services/http';

export const http = httpInitializer();
export const store = storeInitializer(http);
export const i18n = i18nInitializer(http.defaults.baseURL || '');
