export default {
  appName: 'RatCho',
  loginUrl: '/sign-in',
  loggedInUrl: '/categories',
  gridSize: 20,
  supportedLocales: ['en', 'ru'],
  defaultLocale: 'en',
  moment: {
    serverDate: 'YYYY-MM-DD',
    clientDate: 'L',
  },
  googlePlaceApiKey: 'AIzaSyAiUWqowTYOhPwN0P8SEa8sEFQ8QP7x6eM',
};
