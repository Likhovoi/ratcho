import { connect } from 'react-redux';
import { AuthRoute as AuthRouteLoc, AuthRouteProps } from '@app/components/routes/AuthRoute';
import { Store } from '@app/types/store';

const mapStateToProps = function (state: Store): AuthRouteProps {
  return {
    isAuthenticated: Boolean(state.user.account),
  };
};

const connector = connect(mapStateToProps);

export const AuthRoute = connector(AuthRouteLoc);
