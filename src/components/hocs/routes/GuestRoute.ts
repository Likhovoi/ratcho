import { connect } from 'react-redux';
import { GuestRoute as GuestRouteLoc, GuestRouteProps } from '@app/components/routes/GuestRoute';
import { Store } from '@app/types/store';

const mapStateToProps = function (state: Store): GuestRouteProps {
  return {
    isAuthenticated: Boolean(state.user.account),
  };
};

const connector = connect(mapStateToProps);

export const GuestRoute = connector(GuestRouteLoc);
