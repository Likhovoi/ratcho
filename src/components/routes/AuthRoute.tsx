import * as React from 'react';
import { Redirect, Route } from 'react-router-dom';
import config from '@app/config';
import { RouteComponentProps, RouteProps } from 'react-router';

function wrap<P>(Component: React.ComponentType<P>,
                 isAuthenticated: boolean): React.SFC<RouteComponentProps<P>> {
  return ((props: RouteComponentProps<P>) => {
    const {match, location, history, staticContext, ...componentProps} = props;
    if (isAuthenticated) {
      return <Component {...componentProps} />;
    } else {
      const to = {
        pathname: config.loginUrl,
        state: {from: location}
      };
      return <Redirect to={to}/>;
    }
  }) as React.SFC<RouteComponentProps<P>>;
}

export interface AuthRouteProps {
  isAuthenticated: boolean;
}

export const AuthRoute: React.StatelessComponent<AuthRouteProps & RouteProps> = (props) => {
  const {component, isAuthenticated, ...routeProps} = props;
  if (component) {
    return <Route {...routeProps} render={wrap(component, isAuthenticated)}/>;
  }
  return <Route {...routeProps} component={component}/>;
};
