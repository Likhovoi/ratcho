import * as React from 'react';
import { Redirect, Route } from 'react-router-dom';
import config from '@app/config';
import { RouteComponentProps, RouteProps } from 'react-router';

function wrap<P>(Component: React.ComponentType<P>,
                 isAuthenticated: boolean): React.SFC<RouteComponentProps<P>> {
  return ((props: RouteComponentProps<P>) => {
    const {match, location, history, staticContext, ...componentProps} = props;
    if (isAuthenticated) {
      const to = {
        pathname: config.loggedInUrl,
        state: {from: location}
      };
      return <Redirect to={to}/>;
    } else {
      return <Component {...componentProps} />;
    }
  }) as React.StatelessComponent<RouteComponentProps<P>>;
}

export interface GuestRouteProps {
  isAuthenticated: boolean;
}

export const GuestRoute: React.StatelessComponent<GuestRouteProps & RouteProps> = (props) => {
  const {component, isAuthenticated, ...routeProps} = props;
  if (component) {
    return <Route {...routeProps} render={wrap(component, isAuthenticated)}/>;
  }
  return <Route {...routeProps} component={component}/>;
};
