import * as React from 'react';
import { Switch, Redirect } from 'react-router-dom';
import { GuestRoute } from '@app/components/hocs/routes/GuestRoute';
import { AuthRoute } from '@app/components/hocs/routes/AuthRoute';
import { BrowserRouter } from 'react-router-dom';
import { Provider as StoreProvider, connect } from 'react-redux';
import { store } from '@app/services';
import { Store as State } from '@app/types/store';
import userAC from '@app/store/user/ac';
// pages
import { SignIn } from '@app/components/ui/pages/sign-in/SignIn.hoc';
import { SignUp } from '@app/components/ui/pages/sign-up/SignUp.hoc';
import { ThingCategoriesList } from '@app/components/ui/pages/thing-categories-list/ThingCategoriesList.hoc';
import { ThingCategoryCreate } from '@app/components/ui/pages/thing-category-create/ThingCategoryCreate.hoc';
import { ThingCategoryEdit } from '@app/components/ui/pages/thing-category-edit/ThingCategoryEdit.hoc';

export class Root extends React.Component {
  state = {
    ready: false,
  };

  constructor(props: any) {
    super(props);
    const token: string = store.getState().auth.token;
    const loadUserAccountAction = userAC.accountLoad.request({ token });
    store.dispatch(loadUserAccountAction);
    const unsubscribe = store.subscribe(() => {
      const state: State = store.getState();
      if (state.user.accountLoad.received || state.user.accountLoad.rejected) {
        this.setState({ ready: true });
        unsubscribe();
      }
    });
  }

  render() {
    if (!this.state.ready) {
      return '';
    }

    return (
      <StoreProvider store={store}>
        <BrowserRouter>
          <Switch>
            <AuthRoute path="/categories" exact={true} component={ThingCategoriesList}/>
            <AuthRoute path="/categories/new" exact={true} component={ThingCategoryCreate}/>
            <AuthRoute path="/categories/edit/:id" exact={true} component={ThingCategoryEdit}/>

            <GuestRoute path="/sign-in" exact={true} component={SignIn}/>
            <GuestRoute path="/sign-up" exact={true} component={SignUp}/>

            <Redirect from="/" exact={true} to="/profile"/>
          </Switch>
        </BrowserRouter>
      </StoreProvider>
    );
  }
}
