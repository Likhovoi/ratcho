import * as React from 'react';
import { Navbar, NavbarBrand, Collapse, Nav, NavItem, NavLink } from 'reactstrap';
import config from '@app/config';
import * as classnames from 'classnames';

type MainLayoutProps = {
  className?: string;
};

export class MainLayout extends React.Component<MainLayoutProps> {
  render() {
    return (
      <div className={classnames(this.props.className)}>
        <Navbar color="light" light={true} expand="md">
          <NavbarBrand href="/">{config.appName}</NavbarBrand>
          <Collapse isOpen={true} navbar={true}>
            <Nav className="ml-auto" navbar={true}>
              <NavItem>
                <NavLink href="/sign-in">Sign In</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/sign-up">Sign Up</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
        <div className="container">
          {this.props.children}
        </div>
      </div>
    );
  }
}
