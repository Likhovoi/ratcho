import * as React from 'react';
import { InjectedTranslateProps } from 'react-i18next';
import { MainLayout } from '@app/components/ui/layouts/MainLayout';
import {
    InjectedFormProps, 
    FieldArray, 
    GenericFieldArray,
    SubmissionError,
} from 'redux-form';
import { 
    Group, 
    Field, 
    Rule,
    Category, 
} from '@app/types/store/thingCategories';
import { Form, Button } from 'reactstrap';
import { Input } from '@app/components/ui/widgets/redux-form/Input';
import { 
    OwnProps as GroupsOwnProps,
    GroupsSection, 
} from '@app/components/ui/shared/things-categories/GroupsSection';
import { 
    OwnProps as FieldsOwnProps, 
    FieldsSection 
} from '@app/components/ui/shared/things-categories/FieldsSection';
import { 
    OwnProps as RulesOwnProps, 
    RulesSection 
} from '@app/components/ui/shared/things-categories/RulesSection';
import { ValidationError } from '@app/errors/ValidationError';
import { fromDots } from '@app/utils/obj';
import { Redirect, Link } from 'react-router-dom';
import './FormPage.css';

// Types

export type StoreStateProps = {
    saveError: Error | null;
    isSaveFailed: boolean;
    isSaved: boolean;
    formValues: FormValues | undefined;
};

export type StoreActionsProps = {
    save: (data: Category) => void;
    resetReqState: () => void;
    addGroup: () => void;
    rmGroup: (index: number) => void;
    addField: (groupIndex: number) => void;
    rmField: (groupIndex: number, fieldIndex: number) => void;
    addRule: (groupIndex: number, fieldIndex: number) => void;
    rmRule: (groupIndex: number, fieldIndex: number, ruleIndex: number) => void;
    resetRules: (groupIndex: number, fieldIndex: number) => void;
};

export type FormValues = Category;
export type StoreProps = StoreStateProps & StoreActionsProps;
export type FormProps = InjectedFormProps<FormValues, StoreProps>;
export type Props = FormProps & StoreProps & InjectedTranslateProps;

type State = {
    selectedGroupIndex: number;
    selectedFieldIndex: number;
    form?: {
        invalidated: (error: ValidationError) => void;
        saved: () => void;
    }
};

// Component

/**
 * TODO: attract user attention to category input when there are errors inside of it 
 * but user does not see them on current view
 */
export class FormPage extends React.Component<Props> {
    state: State = {
        selectedGroupIndex: 0,
        selectedFieldIndex: 0,
    };

    static getDerivedStateFromProps(nextProps: Props, prevState: State) {
        const { selectedFieldIndex, selectedGroupIndex, form } = prevState;
        if (nextProps.formValues) {
            if (!nextProps.formValues.groups[selectedGroupIndex]) {
                prevState = Object.assign(prevState, {
                    selectedGroupIndex: selectedGroupIndex - 1,
                    selectedFieldIndex: 0,
                });
            } else if (!nextProps.formValues.groups[selectedGroupIndex].fields[selectedFieldIndex]) {
                prevState = Object.assign(prevState, {
                    selectedFieldIndex: selectedFieldIndex - 1,
                });
            }
        }

        if (nextProps.isSaveFailed) {
            if (form) {
                if (nextProps.saveError instanceof ValidationError) {
                    nextProps.resetReqState();
                    form.invalidated(nextProps.saveError);
                } else {
                    nextProps.resetReqState();
                    form.saved();
                }
            }
            prevState = Object.assign(prevState, {
                form: undefined,
            });
        } else if (nextProps.isSaved) {
            if (form) {
                nextProps.resetReqState();
                form.saved();
            }
            prevState = Object.assign(prevState, {
                form: undefined,
            });
        }

        return prevState;
    }

    constructor(props: Props) {
        super(props);
        this.turnGroup = this.turnGroup.bind(this);
        this.turnField = this.turnField.bind(this);
        this.addField = this.addField.bind(this);
        this.rmField = this.rmField.bind(this);
        this.addRule = this.addRule.bind(this);
        this.rmRule = this.rmRule.bind(this);
        this.resetRules = this.resetRules.bind(this);
        this.save = this.save.bind(this);
    }

    turnGroup(index: number) {
        this.setState({ selectedGroupIndex: index });
    }

    turnField(index: number) {
        this.setState({ selectedFieldIndex: index });
    }

    addField() {
        this.props.addField(this.state.selectedGroupIndex);
    }

    rmField(index: number) {
        this.props.rmField(this.state.selectedGroupIndex, index);
    }

    addRule() {
        this.props.addRule(this.state.selectedGroupIndex, this.state.selectedFieldIndex);
    }

    rmRule(index: number) {
        this.props.rmRule(this.state.selectedGroupIndex, this.state.selectedFieldIndex, index);
    }

    resetRules(index: number) {
        this.props.resetRules(this.state.selectedGroupIndex, index);
    }

    save(values: FormValues) {
        let request: FormValues = { ...values };
        if (this.props.initialValues.id) {
            request.id = this.props.initialValues.id;
        }
        this.props.save(request);
        return new Promise((resolve, reject) => {
            this.setState({
                form: {
                    saved: resolve,
                    invalidated: (error: ValidationError) => {
                        const messages = fromDots(error.getMessages());
                        reject(new SubmissionError(messages));
                    },
                }
            });
        });
    }

    render() {
        if (!this.props.formValues) {
            return '';
        }

        if (this.props.isSaved) {
            return (<Redirect to="/categories" />);
        }

        const Groups = FieldArray as new () => GenericFieldArray<Group, GroupsOwnProps>;
        const Fields = FieldArray as new () => GenericFieldArray<Field, FieldsOwnProps>;
        const Rules = FieldArray as new () => GenericFieldArray<Rule, RulesOwnProps>;

        const groupInd: number = this.state.selectedGroupIndex;
        const fieldInd: number = this.state.selectedFieldIndex;
        const group: Group = this.props.formValues.groups[groupInd];

        return (
            <MainLayout className="thing-category-create-root">
                <Form noValidate={true} onSubmit={this.props.handleSubmit(this.save)}>
                    <div className="row">
                        <div className="col-4">
                            <Input type="text" name="name" />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col groups">
                            <Groups 
                                component={GroupsSection}
                                name="groups" 
                                selectedIndex={groupInd}
                                addGroup={this.props.addGroup}
                                rmGroup={this.props.rmGroup}
                                turnGroup={this.turnGroup}
                            />
                            <div className="actions">
                                <Link to="/categories" className="btn btn-outline-dark">
                                    {this.props.t('common:back')}
                                </Link>
                                <Button type="submit" color="success" outline={true}>Save</Button>
                            </div>
                        </div>
                        <div className="col fields">
                            <Fields 
                                component={FieldsSection}
                                name={`groups[${groupInd}].fields`}
                                selectedIndex={fieldInd}
                                addField={this.addField}
                                turnField={this.turnField}
                                rmField={this.rmField}
                                resetRules={this.resetRules}
                            />
                        </div>
                        <div className="col rules">
                            <Rules
                                component={RulesSection}
                                name={`groups[${groupInd}].fields[${fieldInd}].rules`}
                                addRule={this.addRule}
                                rmRule={this.rmRule}
                                field={group.fields[fieldInd]}
                            />
                        </div>
                    </div>
                </Form>
            </MainLayout>
        );
    }
}
