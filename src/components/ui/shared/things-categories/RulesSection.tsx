import * as React from 'react';
import decorate from '@app/utils/decoratorsFlow';
import { WrappedFieldArrayProps } from 'redux-form';
import { Rule, Field } from '@app/types/store/thingCategories';
import { TranslateHocProps } from 'react-i18next/src/translate';
import { translate, InjectedTranslateProps } from 'react-i18next';
import { Button } from 'reactstrap';
import { IoIosPlusEmpty, IoIosCloseEmpty } from 'react-icons/lib/io';
import { Input } from '@app/components/ui/widgets/redux-form/Input';
import { DatePicker } from '@app/components/ui/widgets/redux-form/DatePicker';
import * as classnames from 'classnames';

// Item Component

type ItemProps = {
    ruleName: string;
    index: number;
    field: Field;
    cantDelete: boolean;
    rmRule: (index: number) => void;
};

class ItemComponent extends React.Component<ItemProps> {
    constructor(props: ItemProps) {
        super(props);
        this.rmRule = this.rmRule.bind(this);
    }

    rmRule() {
        if (this.props.cantDelete) {
            return;
        }
        this.props.rmRule(this.props.index);
    }

    render() {
        const ruleName = this.props.ruleName;

        const RmBtn: React.ReactElement<void> = (
            <Button
                color="link"
                className={classnames('rm-rule', {'not-allowed': this.props.cantDelete})}
                onMouseDown={this.rmRule}
                type="button"
            >
                <IoIosCloseEmpty size={16} color="black" />
            </Button>
        );

        if (this.props.field.type === 'numbers') {
            return (
                <div className="field-rule">
                    {RmBtn}
                    <Input type="number" name={`${ruleName}.from_number`} placeholder="common:from" />
                    <Input type="number" name={`${ruleName}.to_number`} placeholder="common:to" />
                    <Input type="number" name={`${ruleName}.rate`} placeholder="things:rate" />
                </div>
            );
        } else if (this.props.field.type === 'values') {
            return (
                <div className="field-rule">
                    {RmBtn}               
                    <Input name={`${ruleName}.value`} placeholder="common:label" />
                    <Input type="number" name={`${ruleName}.rate`} placeholder="things:rate" />
                </div>
            );
        } else if (this.props.field.type === 'dates') {
            return (
                <div className="field-rule">
                    {RmBtn}            
                    <DatePicker name={`${ruleName}.from_date`} placeholder="common:from" />
                    <DatePicker name={`${ruleName}.to_date`} placeholder="common:to" />
                    <Input type="number" name={`${ruleName}.rate`} placeholder="things:rate" />
                </div>
            );
        } else {
            return '';
        }
    }
}

// Main Component

export type OwnProps = {
    addRule: () => void;
    rmRule: (index: number) => void;
    field: Field;
};

type ControlProps = WrappedFieldArrayProps<Rule>;

type RulesSectionProps = OwnProps & ControlProps & InjectedTranslateProps;

class RulesSectionLoc extends React.Component<RulesSectionProps> {
    render() {
        return (
            <div>
                <Button color="link" onClick={this.props.addRule} className="add-rule">
                    <IoIosPlusEmpty size={30} color="black" />
                </Button>
                { this.props.fields.map((ruleName, index) => (
                    <ItemComponent 
                        key={index}
                        ruleName={ruleName}
                        index={index}
                        field={this.props.field}
                        rmRule={this.props.rmRule}
                        cantDelete={this.props.fields.length < 2}
                    />
                )) }
            </div>
        );
    }
}

// Decorators

export const RulesSection = decorate(RulesSectionLoc)
.wrap<ControlProps & OwnProps & TranslateHocProps>((target) => {
    return translate(['common', 'things'])(target);
})
.release();
