import * as React from 'react';
import decorate from '@app/utils/decoratorsFlow';
import { WrappedFieldArrayProps } from 'redux-form';
import { Field } from '@app/types/store/thingCategories';
import { TranslateHocProps } from 'react-i18next/src/translate';
import { translate, InjectedTranslateProps, TranslationFunction } from 'react-i18next';
import { Button } from 'reactstrap';
import { IoIosPlusEmpty, IoIosCloseEmpty } from 'react-icons/lib/io';
import { Input } from '@app/components/ui/widgets/redux-form/Input';
import * as classnames from 'classnames';

// Item Component

type ItemProps = {
    index: number;
    active: boolean;
    fieldName: string;
    cantDelete: boolean;
    turnField: (index: number) => void;
    rmField: (index: number) => void;
    resetRules: (index: number) => void;
    t: TranslationFunction;
};

class ItemComponent extends React.Component<ItemProps> {
    constructor(props: ItemProps) {
        super(props);
        this.rmField = this.rmField.bind(this);
        this.turnField = this.turnField.bind(this);
        this.resetRules = this.resetRules.bind(this);
        this.preventIfInactive = this.preventIfInactive.bind(this);
    }

    rmField(e: React.SyntheticEvent<any>) {
        e.stopPropagation();
        if (this.props.cantDelete) {
            return;
        }
        this.props.rmField(this.props.index);
    }

    turnField() {
        this.props.turnField(this.props.index);
    }

    resetRules() {
        this.props.resetRules(this.props.index);
    }

    preventIfInactive(e: React.SyntheticEvent<any>) {
        if (!this.props.active) {
            e.preventDefault();
        }
    }

    render() {
        const className = classnames('field', { 'active': this.props.active });
        const rmBtnClass = classnames('rm-field', { 'not-allowed': this.props.cantDelete });
        return (
            <div className={className} onMouseDown={this.turnField}>
                <Button color="link" className={rmBtnClass} onMouseDown={this.rmField} type="button">
                    <IoIosCloseEmpty size={16} color="black" />
                </Button>
                <Input name={`${this.props.fieldName}.name`} placeholder="common:name" />
                <Input
                    type="select"
                    name={`${this.props.fieldName}.type`}
                    onChange={this.resetRules}
                    onMouseDown={this.preventIfInactive}
                >
                    <option value="values">{this.props.t('things:set')}</option>
                    <option value="numbers">{this.props.t('things:numbers')}</option>
                    <option value="dates">{this.props.t('things:dates')}</option>
                </Input>
            </div>
        );
    }
}

// Main Component

export type OwnProps = {
    selectedIndex: number;
    addField: () => void;
    turnField: (index: number) => void;
    rmField: (index: number) => void;
    resetRules: (index: number) => void;
};

type ControlProps = WrappedFieldArrayProps<Field>;

type FieldsSectionProps = OwnProps & ControlProps & InjectedTranslateProps;

class FieldsSectionLoc extends React.Component<FieldsSectionProps> {
    render() {
        return (
            <div>
                <Button color="link" onClick={this.props.addField} className="add-field">
                    <IoIosPlusEmpty size={30} color="black" />
                </Button>
                { this.props.fields.map((fieldName, index) => (
                    <ItemComponent 
                        key={index}
                        index={index}
                        active={this.props.selectedIndex === index}
                        fieldName={fieldName}
                        turnField={this.props.turnField}
                        rmField={this.props.rmField}
                        resetRules={this.props.resetRules}
                        t={this.props.t}
                        cantDelete={this.props.fields.length < 2}
                    />
                )) }
            </div>
        );
    }
}

// Decorators

export const FieldsSection = decorate(FieldsSectionLoc)
.wrap<ControlProps & OwnProps & TranslateHocProps>((target) => {
    return translate(['common', 'things'])(target);
})
.release();
