import * as React from 'react';
import { WrappedFieldArrayProps } from 'redux-form';
import { Button } from 'reactstrap';
import { InjectedTranslateProps } from 'react-i18next';
import { Group } from '@app/types/store/thingCategories';
import * as classnames from 'classnames';
import { Input } from '@app/components/ui/widgets/redux-form/Input';
import decorate from '@app/utils/decoratorsFlow';
import { TranslateHocProps } from 'react-i18next/src/translate';
import { translate } from 'react-i18next';
import { IoIosPlusEmpty, IoIosCloseEmpty } from 'react-icons/lib/io';

// Item Component

type ItemProps = {
    active: boolean;
    index: number;
    group: Group;
    controlName: string;
    turnGroup: (index: number) => void;
    rmGroup: (index: number) => void;
};

class ItemComponent extends React.Component<ItemProps> {
    constructor(props: ItemProps) {
        super(props);
        this.turnGroup = this.turnGroup.bind(this);
        this.rmGroup = this.rmGroup.bind(this);
    }

    turnGroup() {
        this.props.turnGroup(this.props.index);
    }

    rmGroup() {
        this.props.rmGroup(this.props.index);
    }

    render () {
        const placeholder = this.props.group.is_general ? 'things:general_fields' : '';
        const className = classnames('group', {
            active: this.props.active
        });
        return (
            <div className={className} onMouseDown={this.turnGroup}>
                {!this.props.group.is_general && (
                    <Button color="link" className="rm-group" onMouseDown={this.rmGroup} type="button">
                        <IoIosCloseEmpty size={16} color="black" />
                    </Button>
                )}
                <Input
                    name={`${this.props.controlName}.name`}
                    disabled={this.props.group.is_general}
                    placeholder={placeholder}
                />
            </div>
        );
    }
}

// Main Component

export type OwnProps = {
    selectedIndex: number;
    addGroup: () => void;
    rmGroup: (index: number) => void;
    turnGroup: (index: number) => void;
};

type ControlProps = WrappedFieldArrayProps<Group>;

type Props = ControlProps & OwnProps & InjectedTranslateProps;

class GroupsSectionLoc extends React.Component<Props> {
    constructor(props: Props) {
        super(props);
        this.rmGroup = this.rmGroup.bind(this);
    }

    rmGroup(index: number, e: React.SyntheticEvent<any>) {
        e.stopPropagation();
        this.props.rmGroup(index);
    }

    render() {
        return (
            <div>
                <Button color="link" onClick={this.props.addGroup} className="add-group" type="button">
                    <IoIosPlusEmpty size={30} color="black" />
                </Button>
                {this.props.fields.map((groupControlName, index) => (
                    <ItemComponent 
                        key={index} 
                        active={this.props.selectedIndex === index} 
                        index={index} 
                        group={this.props.fields.get(index)}
                        controlName={groupControlName}
                        rmGroup={this.props.rmGroup}
                        turnGroup={this.props.turnGroup}
                    />
                ))}
            </div>
        );
    }
}

// Decorators

export const GroupsSection = decorate(GroupsSectionLoc)
.wrap<ControlProps & OwnProps & TranslateHocProps>((target) => {
    return translate(['things'])(target);
})
.release();
