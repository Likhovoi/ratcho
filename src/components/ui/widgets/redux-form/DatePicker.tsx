import * as React from 'react';
import DatePickerBlank from 'react-datepicker';
import { WrappedFieldProps, Field, BaseFieldProps } from 'redux-form';
import decorate from '@app/utils/decoratorsFlow';
import { translate } from 'react-i18next';
import { TranslateHocProps } from 'react-i18next/src/translate';
import { InjectedTranslateProps } from 'react-i18next';
import { isMoment } from 'moment';
import * as moment from 'moment';
import config from '@app/config';

export type CustomProps = {
    name: string;
    placeholder: string;
};

export type WrappedDatePickerProps = CustomProps & WrappedFieldProps & InjectedTranslateProps;

export const WrappedDatePicker: React.SFC<WrappedDatePickerProps> = (props) => {
    if (isMoment(props.input.value)) {
        props.input.value = props.input.value.format('L');
    }
    let placeholder: string = '';
    if (props.placeholder) {
        placeholder = props.t(props.placeholder);
    }
    let { value, onChange, onBlur } = props.input;
    if (value && !isMoment(value)) {
        value = moment(value, config.moment.clientDate);
    }

    return (
        <DatePickerBlank 
            name={props.name} 
            onBlur={onBlur}
            onChange={onChange}
            selected={value || null}
            isClearable={true}
            className="form-control" 
            placeholderText={placeholder}
        />
    );
};

export const DatePicker = decorate(WrappedDatePicker)
.wrap<CustomProps & WrappedFieldProps & TranslateHocProps>((target) => {
    return translate()(target);
})
.wrap<CustomProps & BaseFieldProps>((target) => {
  const wrapper: React.SFC<CustomProps> = (props) => {
    return (<Field {...props} component={target} />);
  };
  return wrapper;
})
.release();
