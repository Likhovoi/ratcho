import * as React from 'react';
import { Field, WrappedFieldProps, InjectedFormProps, BaseFieldProps } from 'redux-form';
import { Input as BootstrapInput, FormFeedback } from 'reactstrap';
import { InputType } from 'reactstrap/lib/Input';
import { translate, InjectedTranslateProps, InjectedI18nProps } from 'react-i18next';
import { TranslateHocProps } from 'react-i18next/src/translate';
import decorate from '@app/utils/decoratorsFlow';

export type InputProps = {
  name: string;
  options?: { [key: string]: string };
  className?: string;
  type?: InputType;
  controlId?: string;
  placeholder?: string;
  translate?: boolean;
  disabled?: boolean;
  onMouseDown?: (e: React.MouseEvent<any>) => void;
};

export type WrappedInputProps = InputProps & WrappedFieldProps & InjectedTranslateProps;

/**
 * Low-level Input component
 */
export const WrappedInputLoc: React.SFC<WrappedInputProps> = (props) => {
  const defaultProps = {
    translate: true,
    type: 'text',
    disabled: false,
  };
  props = Object.assign(defaultProps, props);

  // if (props.meta.invalid) {
  //   console.log('INPUT', props.input.name, props.meta.error);
  // }

  let placeholder: string = '';
  if (props.placeholder) {
    if (props.translate) {
      placeholder = props.t(props.placeholder);
    } else {
      placeholder = props.placeholder;
    }
  }

  return (
    <BootstrapInput
      {...props.input}
      id={props.controlId}
      type={props.type}
      placeholder={placeholder}
      className={props.className}
      invalid={props.meta.invalid}
      disabled={props.disabled}
      onMouseDown={props.onMouseDown}
      autoComplete="off"
    >
    {props.options ? Object.keys(props.options).map((optionName) => (
      <option value={optionName}>{props.options && props.options[optionName]}</option>
    )) : props.children }
    </BootstrapInput>
  );
};

export const Input = decorate(WrappedInputLoc)
.wrap<InputProps & WrappedFieldProps & TranslateHocProps>((target) => {
  return translate()(target);
})
.wrap<InputProps & TranslateHocProps & BaseFieldProps>((target) => {
  const wrapper: React.SFC<InputProps & TranslateHocProps> = (props) => {
    return (<Field {...props} component={target} />);
  };
  return wrapper;
})
.release();
