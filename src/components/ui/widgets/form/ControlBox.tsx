import * as React from 'react';
import { FormGroup, Label, FormFeedback } from 'reactstrap';
import { translate, InjectedTranslateProps } from 'react-i18next';

export type ControlBoxRawProps = {
  boxClass?: string;
  labelClass?: string;
  controlId?: string;
  label?: string;
  i18n?: boolean;
  errors?: string[];
  showErrors?: boolean;
  required?: boolean;
};

export type ControlBoxProps = ControlBoxRawProps & InjectedTranslateProps;

/**
 * Low level component that combines InputGroup, Label, FormFeedback components of
 * bootstrap library into one
 */
export const ControlBoxLoc: React.SFC<ControlBoxProps> = (props) => {
  const defaultProps = {
    required: false,
    showErrors: true,
    i18n: true,
    errors: [],
  };
  
  props = Object.assign(defaultProps, props);

  let ControlLabel: React.ReactElement<void>|undefined;
  let ControlFeedback: React.ReactElement<void>[]|undefined;

  if (props.label) {
    let label: string = '';
    if (props.i18n) {
      label = props.t(props.label);
    } else {
      label = props.label;
    }
    ControlLabel = (
      <Label for={props.controlId} className={props.labelClass}>
        {label}
        {props.required ? <span className="text-danger"> *</span> : ''}
      </Label>
    );
  }

  if (props.showErrors && Array.isArray(props.errors)) {
    ControlFeedback = props.errors.map((error, k) => (
      <FormFeedback key={k}>{error}</FormFeedback>
    ));
  }

  return (
    <FormGroup>
      {ControlLabel}
      {props.children}
      {ControlFeedback}
    </FormGroup>
  );
};

/**
 * ControlBox wrapped by Intl
 */
export const ControlBox = translate()(ControlBoxLoc);
