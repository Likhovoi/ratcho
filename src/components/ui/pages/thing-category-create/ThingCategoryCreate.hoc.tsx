import { Cover } from '@app/utils/decoratorsFlow';
import { translate } from 'react-i18next';
import ac from '@app/store/thingsCategories/ac';
import { FormPage, FormValues, StoreStateProps, StoreActionsProps } 
    from '@app/components/ui/shared/things-categories/FormPage';
import { reduxForm, getFormValues, change, arrayPush, arrayRemove } from 'redux-form';
import { Store } from '@app/types/store';
import { connect, Dispatch } from 'react-redux';
import { Group, Field, Rule, Category } from '@app/types/store/thingCategories';

const formName = 'ThingCategoryCreateForm';

const withStore = (() => {
    const mapStateToProps = (state: Store): StoreStateProps => {
        const formValues = getFormValues(formName)(state) as FormValues | undefined;
        return {
            formValues,
            saveError: state.thingsCategories.save.error,
            isSaveFailed: state.thingsCategories.save.rejected,
            isSaved: state.thingsCategories.save.received,
        };
    };
    const mapDispatchToProps = (dispatch: Dispatch<Store>): StoreActionsProps => {
        return {
            save: (data: Category) => { 
                dispatch(ac.save.request(data));
            },
            resetReqState: () => {
                dispatch(ac.save.reset());
            },
            addGroup: () => {
                const newGroup: Group = {
                    is_general: false,
                    name: '',
                    fields: [ { type: 'values', name: '', rules: [ { rate: null } ], }, ],
                };
                dispatch(arrayPush(formName, 'groups', newGroup));
            },
            rmGroup: (index: number) => {
                dispatch(arrayRemove(formName, 'groups', index));
            },
            addField: (categoryIndex: number) => {
                const newField: Field = {
                    type: 'values',
                    name: '',
                    rules: [ { rate: null } ]
                };
                dispatch(arrayPush(formName, `groups[${categoryIndex}].fields`, newField));
            },
            rmField: (categoryIndex: number, fieldIndex: number) => {
                dispatch(arrayRemove(formName, `groups[${categoryIndex}].fields`, fieldIndex));
            },
            addRule: (categoryIndex: number, fieldIndex: number) => {
                const newRule: Rule = { rate: null };
                const key = `groups[${categoryIndex}].fields[${fieldIndex}].rules`;
                dispatch(arrayPush(formName, key, newRule));
            },
            rmRule: (categoryIndex: number, fieldIndex: number, ruleIndex: number) => {
                const key = `groups[${categoryIndex}].fields[${fieldIndex}].rules`;
                dispatch(arrayRemove(formName, key, ruleIndex));
            },
            resetRules: (categoryIndex: number, fieldIndex: number) => {
                const rules: Rule[] = [ { rate: null } ];
                const key = `groups[${categoryIndex}].fields[${fieldIndex}].rules`;
                dispatch(change(formName, key, rules));
            },
        };
    };
    return connect(mapStateToProps, mapDispatchToProps);
})();

const withForm = reduxForm<FormValues>({ 
    form: formName,
    initialValues: {
        groups: [
            { 
                is_general: true,
                name: '',
                fields: [ { type: 'values', name: '', rules: [ { rate: null } ] } ]
            },
        ]
    },
});

export const ThingCategoryCreate = (new Cover())
    .layer(withForm)
    .layer(withStore)
    .layer(translate(['common', 'things']))
    .cover(FormPage);
