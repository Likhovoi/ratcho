import * as React from 'react';
import { MainLayout } from '@app/components/ui/layouts/MainLayout';

export class ThingCreate extends React.Component {
    render() {
        return (
            <MainLayout className="thing-create-root">
                <div>
                    <div className="row">
                        <div className="col-md-12">
                            <table className="grid-inputs">
                                <thead>
                                    <tr>
                                        <td className="hidden"/>
                                        <td><input type="text"/></td>
                                        <td><input type="text"/></td>
                                        <td><input type="text"/></td>
                                        <td className="add-parameter"/>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td className="add-field"/>
                                        <td><input type="text"/></td>
                                        <td><input type="text"/></td>
                                        <td><input type="text"/></td>
                                        <td className="hidden"/>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </MainLayout>
        );
    }
}
