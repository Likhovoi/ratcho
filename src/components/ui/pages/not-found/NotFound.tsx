import * as React from 'react';
import { MainLayout } from '@app/components/ui/layouts/MainLayout';

export class NotFound extends React.Component {
  render() {
    return (
      <MainLayout>
        Not Found
      </MainLayout>
    );
  }
}
