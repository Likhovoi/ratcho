import { Cover } from '@app/utils/decoratorsFlow';
import { translate } from 'react-i18next';
import { 
    ThingCategoriesList as Blank, 
    StoreActionsProps,
    StoreStateProps
} from '@app/components/ui/pages/thing-categories-list/ThingCategoriesList';
import { connect } from 'react-redux';
import { Store } from '@app/types/store';
import { Dispatch } from 'redux';
import ac from '@app/store/thingsCategories/ac';

const withStore = (() => {
    const mapStateToProps = (store: Store): StoreStateProps => {
        return {
            isRequest: store.thingsCategories.search.request,
            isRejected: store.thingsCategories.search.rejected,
            categories: store.thingsCategories.categories,
        };
    };
    const mapDispatchToProps = (dispatch: Dispatch<Store>): StoreActionsProps => {
        return {
            search: () => { 
                dispatch(ac.search.request({}));
            },
            removeCategory: (id: number) => {
                dispatch(ac.remove.request(id));
            },
        };
    };
    return connect(mapStateToProps, mapDispatchToProps);
})();

export const ThingCategoriesList = (new Cover)
    .layer(withStore)
    .layer(translate(['common']))
    .cover(Blank);
