import * as React from 'react';
import { MainLayout } from '@app/components/ui/layouts/MainLayout';
import { Trans, InjectedTranslateProps, TranslationFunction } from 'react-i18next';
import { Category } from '@app/types/store/thingCategories';
import { Link } from 'react-router-dom';

export type StoreStateProps = {
  isRequest: boolean;
  isRejected: boolean;
  categories: Category[] | null;
};

export type StoreActionsProps = {
  search: () => void;
  removeCategory: (id: number) => void;
};

export type StoreProps = StoreStateProps & StoreActionsProps;

export type Props = StoreProps & InjectedTranslateProps;

type ThingCategoryTableRowProps = {
  category: Category;
  t: TranslationFunction;
  remove: (id: number) => void;
};

class ThingCategoryTableRow extends React.Component<ThingCategoryTableRowProps> {
  constructor(props: ThingCategoryTableRowProps) {
    super(props);
    this.remove = this.remove.bind(this);
  }

  remove () {
    if (this.props.category.id) {
      this.props.remove(this.props.category.id);
    }
  }

  render () {
    return (
      <tr key={this.props.category.id}>
        <td>{this.props.category.name}</td>
        <td>
          <Link to={`/categories/edit/${this.props.category.id}`} className="btn btn-link">
            {this.props.t('common:edit')}
          </Link>
          <button className="btn btn-link" onClick={this.remove}>{this.props.t('common:delete')}</button>
        </td>
      </tr>  
    );
  }
}

export class ThingCategoriesList extends React.Component<Props> {

  constructor (props: Props) {
    super(props);
    this.removeCategory = this.removeCategory.bind(this);
  }

  componentDidMount() {
    if (!this.props.isRejected && this.props.categories === null) {
      this.props.search();
    }
  }

  removeCategory (id: number) {
    this.props.removeCategory(id);
  }

  render() {
    if (this.props.isRequest || this.props.categories === null) {
      return '';
    }

    return (
      <MainLayout>
        <Link to="categories/new"><Trans i18nKey="common:add"/></Link>
        <br/>
        <table className="table">
          <tbody>
            {this.props.categories && this.props.categories.map((category) => (
              <ThingCategoryTableRow 
                key={category.id} 
                remove={this.props.removeCategory}
                category={category}
                t={this.props.t}
              />
            ))}
          </tbody>
        </table>
      </MainLayout>
    );
  }
}
