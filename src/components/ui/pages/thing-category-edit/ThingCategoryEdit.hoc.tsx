import * as React from 'react';
import { Cover } from '@app/utils/decoratorsFlow';
import { translate } from 'react-i18next';
import ac from '@app/store/thingsCategories/ac';
import { FormPage, FormValues, StoreStateProps, StoreActionsProps } 
    from '@app/components/ui/shared/things-categories/FormPage';
import { reduxForm, getFormValues, change, arrayPush, arrayRemove } from 'redux-form';
import { Store } from '@app/types/store';
import { connect, Dispatch } from 'react-redux';
import { Group, Field, Rule, Category } from '@app/types/store/thingCategories';
import { withRouter, Redirect, RouteComponentProps } from 'react-router-dom';

const formName = 'ThingCategoryEditForm';
type ComponentType<P> = React.ComponentType<P>;
type RouteParams = { id: number; };
type RouteProps = RouteComponentProps<RouteParams>;

type EditStoreStateProps = StoreStateProps & { 
    isSearching: boolean;
    isSearchFailed: boolean;
    isSearched: boolean;
    categories: Category[] | null;
 };

type ExistedStoreActionsProps = StoreActionsProps & {
    load: (id: number) => void,
};

const withStore = (() => {
    const mapStateToProps = (state: Store): EditStoreStateProps => {
        const formValues = getFormValues(formName)(state) as FormValues | undefined;
        return {
            isSearching: state.thingsCategories.search.request,
            isSearchFailed: state.thingsCategories.search.rejected,
            isSearched: state.thingsCategories.search.received,
            saveError: state.thingsCategories.save.error,
            isSaveFailed: state.thingsCategories.save.rejected,
            isSaved: state.thingsCategories.save.received,
            categories: state.thingsCategories.categories,
            formValues,
        };
    };
    const mapDispatchToProps = (dispatch: Dispatch<Store>): ExistedStoreActionsProps => {
        return {
            load: (id: number) => {
                dispatch(ac.search.request({ id }));
            },
            save: (data: Category) => { 
                dispatch(ac.save.request(data));
            },
            resetReqState: () => {
                dispatch(ac.save.reset());
            },
            addGroup: () => {
                const newGroup: Group = {
                    is_general: false,
                    name: '',
                    fields: [ { type: 'values', name: '', rules: [ { rate: null } ], }, ],
                };
                dispatch(arrayPush(formName, 'groups', newGroup));
            },
            rmGroup: (index: number) => {
                dispatch(arrayRemove(formName, 'groups', index));
            },
            addField: (categoryIndex: number) => {
                const newField: Field = {
                    type: 'values',
                    name: '',
                    rules: [ { rate: null } ]
                };
                dispatch(arrayPush(formName, `groups[${categoryIndex}].fields`, newField));
            },
            rmField: (categoryIndex: number, fieldIndex: number) => {
                dispatch(arrayRemove(formName, `groups[${categoryIndex}].fields`, fieldIndex));
            },
            addRule: (categoryIndex: number, fieldIndex: number) => {
                const newRule: Rule = { rate: null };
                const key = `groups[${categoryIndex}].fields[${fieldIndex}].rules`;
                dispatch(arrayPush(formName, key, newRule));
            },
            rmRule: (categoryIndex: number, fieldIndex: number, ruleIndex: number) => {
                const key = `groups[${categoryIndex}].fields[${fieldIndex}].rules`;
                dispatch(arrayRemove(formName, key, ruleIndex));
            },
            resetRules: (categoryIndex: number, fieldIndex: number) => {
                const rules: Rule[] = [ { rate: null } ];
                const key = `groups[${categoryIndex}].fields[${fieldIndex}].rules`;
                dispatch(change(formName, key, rules));
            },
        };
    };
    return connect(mapStateToProps, mapDispatchToProps);
})();

function withInitialValues (Target: ComponentType<any>): ComponentType<any> {
    return (props: EditStoreStateProps & ExistedStoreActionsProps & RouteProps) => {
        const categoryId = +props.match.params.id;
        let category: Category | undefined;
        if (props.categories) {
            category = props.categories.find(item => item.id === categoryId);
        }
        if (props.isSearching) {
            return null;
        } else if (props.isSearched && category) {
            return (<Target {...props} initialValues={category} />);
        } else if (props.isSearchFailed || (props.isSearched && !category)) {
            return (<Redirect to="/not-found"/>);
        } else {
            props.load(categoryId);
            return null;
        }
    };
}

export const ThingCategoryEdit = (new Cover())
    .layer(reduxForm({form: formName}))
    .layer(withInitialValues)
    .layer(withStore)
    .layer(withRouter)
    .layer(translate(['common', 'things']))
    .cover(FormPage);
