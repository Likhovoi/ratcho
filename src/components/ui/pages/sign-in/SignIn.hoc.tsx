import * as React from 'react';
import { reduxForm, InjectedFormProps } from 'redux-form';
import { connect, Dispatch } from 'react-redux';
import { Store } from '@app/types/store';
import authAC from '@app/store/auth/ac';
import { SignIn as SignInBlank, FormValues, StoreProps } from '@app/components/ui/pages/sign-in/SignIn';
import { SignInRequest } from '@app/types/store/auth';
import { translate, Trans, InjectedTranslateProps } from 'react-i18next';
import decorate from '@app/utils/decoratorsFlow';
import { TranslateHocProps } from 'react-i18next/src/translate';

export const SignIn = decorate(SignInBlank)
.wrap<InjectedFormProps<FormValues, StoreProps & TranslateHocProps> & StoreProps & TranslateHocProps>((target) => {
  return translate(['common', 'auth', 'person'])(target);
})
.wrap<StoreProps & TranslateHocProps>((target) => {
  return reduxForm<FormValues, StoreProps & TranslateHocProps>({
    form: 'signInForm',
  })(target);
})
.wrap<TranslateHocProps>((target) => {
  const mapStateToProps = (state: Store) => {
    return {
      isSignInRequest: state.auth.signIn.request,
      isSignInReceived: state.auth.signIn.received,
      isSignInRejected: state.auth.signIn.rejected,
      signInError: state.auth.signIn.error,
    };
  };
  const mapDispatchToProps = (dispatch: Dispatch<Store>) => {
    return {
      signIn: (data: SignInRequest) => {
        dispatch(authAC.signIn.request(data));
      },
      signInReset: () => {
        dispatch(authAC.signIn.reset());
      }
    };
  };
  return connect(
    mapStateToProps, 
    mapDispatchToProps,
  )(target);
})
.release();
