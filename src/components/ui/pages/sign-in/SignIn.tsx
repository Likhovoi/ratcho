import * as React from 'react';
import { MainLayout } from '@app/components/ui/layouts/MainLayout';
import { Link } from 'react-router-dom';
import { InjectedFormProps, Field, SubmissionError } from 'redux-form';
import { SignInRequest } from '@app/types/store/auth';
import { ValidationError } from '@app/errors/ValidationError';
import { ControlBox } from '@app/components/ui/widgets/form/ControlBox';
import { Input } from '@app/components/ui/widgets/redux-form/Input';
import { FormGroup, Button, Form } from 'reactstrap';
import { Trans, InjectedTranslateProps } from 'react-i18next';
import { dotsToBrackets } from '@app/ext/redux-form/utils';

export interface FormValues extends SignInRequest {}

export type StoreProps = {
  isSignInRequest: boolean;
  isSignInRejected: boolean;
  isSignInReceived: boolean;
  signInError: Error | null;
  signIn: (data: SignInRequest) => void,
  signInReset: () => void,
};

type SignInProps = InjectedFormProps<FormValues, StoreProps> & StoreProps & InjectedTranslateProps;

type State = {
  form?: {
    invalidated: (error: ValidationError) => void;
    submitted: () => void;
  }
};

export class SignIn extends React.Component<SignInProps> {
  public state: State = {};
  
  static getDerivedStateFromProps(nextProps: SignInProps, prevState: State) {
    if (nextProps.isSignInRejected) {
      if (prevState.form) {
        if (nextProps.signInError instanceof ValidationError) {
          prevState.form.invalidated(nextProps.signInError);
        } else {
          prevState.form.submitted();
        }
      }
      return {};
    } else if (nextProps.isSignInReceived) {
      if (prevState.form) {
        prevState.form.submitted();
      }
      return {};
    }
    return null;
  }

  constructor(props: SignInProps) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(values: FormValues) {
    this.props.signIn(values);
    return new Promise((resolve, reject) => {
      this.setState({
        form: {
          submitted: resolve,
          invalidated: (error: ValidationError) => {
            const messages = dotsToBrackets<FormValues>(error.getMessages());
            reject(new SubmissionError(messages));
          }
        },
      });
    });
  }

  validationMessages(field: string) {
    if (this.props.signInError instanceof ValidationError) {
      return this.props.signInError.getMessages()[field];
    } else {
      return [];
    }
  }

  render() {
    return (
      <MainLayout>
        <div className="row">
          <div className="col-xs-12">
            <h1><Trans i18nKey="auth:sign_in_form" /></h1>
            <Form onSubmit={this.props.handleSubmit(this.onSubmit)} noValidate={true}>
              <ControlBox required={true} errors={this.validationMessages('email')}>
                <Input name="email" type="email" placeholder="person:email" />
              </ControlBox>
              <ControlBox required={true} errors={this.validationMessages('password')}>
                <Input name="password" type="password" placeholder="auth:password" />
              </ControlBox>
              <FormGroup>
                <Button type="submit" color="success">
                  <Trans i18nKey="common:submit" />
                </Button>
              </FormGroup>
              <div>
                <Link to="/sign-up"><Trans i18nKey="auth:not_registered_yet" /></Link>
              </div>
            </Form>
          </div>
        </div>
      </MainLayout>
    );
  }
}
