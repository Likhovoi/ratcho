import * as React from 'react';
import { MainLayout } from '@app/components/ui/layouts/MainLayout';
import { InjectedFormProps, Field, SubmissionError } from 'redux-form';
import { SignUpRequest } from '@app/types/store/auth';
import { ValidationError } from '@app/errors/ValidationError';
import { Link, Redirect } from 'react-router-dom';
import { dotsToBrackets } from '@app/ext/redux-form/utils';
import { Input } from '@app/components/ui/widgets/redux-form/Input';
import { ControlBox } from '@app/components/ui/widgets/form/ControlBox';
import { FormGroup, Button, Form } from 'reactstrap';
import { translate, Trans, InjectedTranslateProps } from 'react-i18next';

export interface FormValues extends SignUpRequest {}

export type StoreProps = {
  signUpError: Error | null;
  isSignUpRequest: boolean;
  isSignUpReceived: boolean;
  isSignUpRejected: boolean;
  signUp: (data: SignUpRequest) => void;
  signUpReset: () => void;
};

type SignUpProps = InjectedFormProps<FormValues, StoreProps> & StoreProps & InjectedTranslateProps;

type State = {
  form?: {
    submitted: () => void;
    invalidated: (error: ValidationError) => void;
  },
};

export class SignUp extends React.Component<SignUpProps> {
  public state: State = {};

  static getDerivedStateFromProps(nextProps: SignUpProps, prevState: State) {
    if (nextProps.isSignUpRejected) {
      if (prevState.form) {
        if (nextProps.signUpError instanceof ValidationError) {
          prevState.form.invalidated(nextProps.signUpError);
        } else {
          prevState.form.submitted();
        }
      }
      return {};
    } else if (nextProps.isSignUpReceived) {
      if (prevState.form) {
        prevState.form.submitted();
      }
      return {};
    }
    return null;
  }

  constructor(props: SignUpProps) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(values: FormValues) {
    this.props.signUp(values);
    return new Promise((resolve, reject) => {
      this.setState({
        form: {
          submitted: resolve, 
          invalidated: (error: ValidationError) => {
            const messages = dotsToBrackets<FormValues>(error.getMessages());
            reject(new SubmissionError(messages));
          },
        },
      });
    });
  }

  validationMessages(field: string) {
    if (this.props.signUpError instanceof ValidationError) {
      return this.props.signUpError.getMessages()[field];
    } else {
      return [];
    }
  }

  render() {
    if (this.props.isSignUpReceived) {
      this.props.signUpReset();
      return (<Redirect to="sign-in" />);
    }

    return (
      <MainLayout>
        <div className="row">
          <div className="col-xs-12">
            <h1><Trans i18nKey="auth:sign_up_form" /></h1>
            <Form onSubmit={this.props.handleSubmit(this.onSubmit)} noValidate={true}>
              <ControlBox required={true} errors={this.validationMessages('name')}>
                <Input name="name" placeholder="person:name" />
              </ControlBox>
              <ControlBox required={true} errors={this.validationMessages('email')}>
                <Input name="email" type="email" placeholder="person:email" />
              </ControlBox>
              <ControlBox required={true} errors={this.validationMessages('password')}>
                <Input name="password" type="password" placeholder="auth:password" />
              </ControlBox>
              <ControlBox required={true} errors={this.validationMessages('password_confirmation')}>
                <Input name="password_confirmation" type="password" placeholder="auth:password_confirmation" />
              </ControlBox>
              <FormGroup>
                <Button type="submit" color="success">
                  <Trans i18nKey="common:submit" />
                </Button>
              </FormGroup>
              <div>
                <Link to="/sign-in"><Trans i18nKey="auth:already_registered" /></Link>
              </div>
            </Form>
          </div>
        </div>
      </MainLayout>
    );
  }
}
