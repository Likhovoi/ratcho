import * as React from 'react';
import { reduxForm, InjectedFormProps } from 'redux-form';
import { connect, Dispatch } from 'react-redux';
import { Store } from '@app/types/store';
import authAC from '@app/store/auth/ac';
import { translate, Trans, InjectedTranslateProps } from 'react-i18next';
import { SignUp as SignUpBlank, FormValues, StoreProps } from '@app/components/ui/pages/sign-up/SignUp';
import { SignUpRequest } from '@app/types/store/auth';
import decorate from '@app/utils/decoratorsFlow';
import { TranslateHocProps } from 'react-i18next/src/translate';

export const SignUp = decorate(SignUpBlank)
.wrap<InjectedFormProps<FormValues, StoreProps & TranslateHocProps> & TranslateHocProps & StoreProps>((target) => {
  return translate(['common', 'auth', 'person'])(target);
})
.wrap<TranslateHocProps & StoreProps>((target) => {
  return reduxForm<FormValues, StoreProps & TranslateHocProps>({
    form: 'signUpForm',
  })(target);
})
.wrap<TranslateHocProps>((target) => {
  const mapStateToProps = (state: Store) => {
    return {
      isSignUpRequest: state.auth.signUp.request,
      isSignUpReceived: state.auth.signUp.received,
      isSignUpRejected: state.auth.signUp.rejected,
      signUpError: state.auth.signUp.error,
    };
  };
  const mapDispatchToProps = (dispatch: Dispatch<Store>) => {
    return {
      signUp: (data: SignUpRequest) => {
        dispatch(authAC.signUp.request(data));
      },
      signUpReset: () => {
        dispatch(authAC.signUp.reset());
      }
    };
  };
  return connect(
    mapStateToProps, 
    mapDispatchToProps
  )(target);
})
.release();
