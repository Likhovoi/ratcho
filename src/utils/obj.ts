import { set } from 'lodash';

export function fromDots(dotted: object) {
    const res = {};
    Object.keys(dotted).forEach(k => {
        set(res, k, dotted[k]);        
    });
    return res;
}
