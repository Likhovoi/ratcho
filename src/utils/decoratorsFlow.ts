import * as React from 'react';

type Component<P = {}> = React.ComponentType<P>;

export default function decorate<F>(target: Component<F>) {
    const wrapper = {
        wrap<R>(fn: (prev: Component<F>) => Component<R>) {
            const newTarget = fn(target);
            return decorate<R>(newTarget);
        },
        release(): Component<F> {
            return target;
        }
    };
    return wrapper;
}

export type Decorator<From, To> = (base: React.ComponentType<From>) => React.ComponentType<To>;

export class Cover<StartProps = {}, ReducedProps = StartProps> {
    decorators: Decorator<any, any>[] = [];

    constructor (decorators: Decorator<any, any>[] = []) {
        this.decorators = decorators;
    }

    layer<NextProps> (decorator: Decorator<ReducedProps, NextProps>) {
        return new Cover<StartProps, ReducedProps & NextProps>(this.decorators.concat([ decorator ]));
    }

    cover<ComponentProps extends StartProps> (component: React.ComponentType<ComponentProps>):
     React.ComponentType<ComponentProps & ReducedProps> {
        for (let decorator of this.decorators) {
            component = decorator(component);
        }
        return component as React.ComponentType<ComponentProps & ReducedProps>;
    }
}
