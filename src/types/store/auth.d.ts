import { ReqState, ReqAction } from '@app/types/redux/boilerplate';
import { Account } from '@app/types/store/user';

// Redux

export type AuthState = {
  signUp: ReqState;
  signIn: ReqState;
  token: string;
};

// Sign In

export type SignInRequest = {
  email: string;
  password: string;
};

export type SignInResponse = Account;

// Sign Up

export type SignUpRequest = {
  email: string;
  name: string;
  password: string;
  password_confirmation: string;
};

export type SignUpResponse = {};
