import { ReqState } from '@app/types/redux/boilerplate';

// Redux

export type ReducerState = {
    search: ReqState;
    save: ReqState;
    remove: ReqState;
    categories: Category[] | null;
};

// Entities

export type SearchFilters = {
    id?: number;
}

export type FieldType = 'numbers' | 'values' | 'dates';

export type Rule = {
    id?: number;
    from_number?: number;
    to_number?: number;
    from_date?: number;
    to_date?: number;
    value?: number|string;
    label?: string;
    rate: number | null;
};

export type Field = {
    id?: number;
    type: FieldType;
    name: string;
    rules: Rule[];
};

export type Group = {
    id?: number;
    is_general: boolean;
    name: string;
    fields: Field[];
};

export type Category = {
    id?: number;
    name: string;
    groups: Group[];
};
