import { ReqState } from "@app/types/redux/boilerplate";

// Redux 

export type UserState = {
    accountLoad: ReqState;
    account: Account | null;
};

// Entities

export type Account = {
    id: number;
    email: string;
    name: string;
    api_token: string;
};

// Load Account

export type AccountLoadRequest = {
    token: string;
};

export type AccountLoadResponse = Account;
