import { Action } from 'redux';

export interface ReqState {
  request: boolean;
  received: boolean;
  rejected: boolean;
  error: Error | null;
}

export interface ReqActionTypes {
  REQUEST: string;
  RECEIVED: string;
  REJECTED: string;
  RESET: string;
}

export interface ReqAction<T> extends Action {
  data: T;
  error?: Error;
}

export interface ReqAC<Req, Rec, Rej> {
  request(data: Req): ReqAction<Req>;
  received(data: Rec): ReqAction<Rec>;
  rejected(data: Rej): ReqAction<Rej>;
  reset(): Action;
}
