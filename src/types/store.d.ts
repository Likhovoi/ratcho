import { FormStateMap } from 'redux-form';
import { AuthState } from '@app/types/store/auth';
import { UserState } from '@app/types/store/user';
import { ReducerState as ThingCategories } from '@app/types/store/thingCategories';

export type Store = {
  form: FormStateMap;
  auth: AuthState;
  user: UserState;
  thingsCategories: ThingCategories;
};
