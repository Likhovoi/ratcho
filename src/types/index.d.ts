export type StrMap<T> = { [key: string]: T };
export type InvalidationMessages = StrMap<string[]>;
export type Diff<T extends string, U extends string> = ({[P in T]: P } & {[P in U]: never } & { [x: string]: never })[T];
export type Minus<T, U> = Pick<T, Diff<keyof T, keyof U>>;