import ACTIONS from '@app/store/actions';
import { initialReqState, nextReqState, switchAction } from '@app/ext/redux/boilerplate';
import { merge } from 'lodash';
import { AnyAction } from 'redux';
import { Account, UserState, AccountLoadResponse } from '@app/types/store/user';
import { ReqAction } from '@app/types/redux/boilerplate';

const initialState = {
  accountLoad: initialReqState(),
  account: null,
};

export default function (state: UserState = initialState, action: ReqAction<any>) {
  let newState: UserState = merge({}, state, {
    accountLoad: nextReqState(state.accountLoad, action, ACTIONS.USER.ACCOUNT_LOAD),
  });
  newState = switchAction(newState, action, {
    [ACTIONS.AUTH.SIGN_OUT]: (a: ReqAction<void>) => ({
      account: null,
    }),
    [ACTIONS.USER.ACCOUNT_LOAD.RECEIVED]: (a: ReqAction<AccountLoadResponse>) => ({
      account: a.data,
    })
  });
  return newState;
}
