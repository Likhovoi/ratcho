import { http } from '@app/services';
import { send } from '@app/ext/axios/api';
import { AccountLoadRequest, AccountLoadResponse } from '@app/types/store/user';

export async function accountLoad(data: AccountLoadRequest): Promise<AccountLoadResponse> {
  return send(http.get<AccountLoadResponse>('user/account', { data }));
}
