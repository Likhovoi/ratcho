import { call, take, fork, put } from 'redux-saga/effects';
import ACTIONS from '@app/store/actions';
import { signIn, signUp } from '@app/store/auth/api';
import userAC from '@app/store/user/ac';
import { ReqAction } from '@app/types/redux/boilerplate';
import { AccountLoadRequest, AccountLoadResponse } from '@app/types/store/user';
import { accountLoad } from '@app/store/user/api';

function* accountFlow() {
  while (true) {
    const action: ReqAction<AccountLoadRequest> = yield take(ACTIONS.USER.ACCOUNT_LOAD.REQUEST);
    try {
      const response: AccountLoadResponse = yield call(accountLoad, action.data);
      yield put(userAC.accountLoad.received(response));
    } catch (error) {
      yield put(userAC.accountLoad.rejected(error));
    }
  }
}

export default function* () {
  yield fork(accountFlow);
}
