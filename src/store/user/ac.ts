import { createReqAC } from '@app/ext/redux/boilerplate';
import actions from '@app/store/actions';
import { AccountLoadRequest, AccountLoadResponse } from '@app/types/store/user';

const accountLoad = createReqAC<AccountLoadRequest, AccountLoadResponse, Error>(actions.USER.ACCOUNT_LOAD);

export default { accountLoad };
