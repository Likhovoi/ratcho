import { createReqAC } from '@app/ext/redux/boilerplate';
import actions from '@app/store/actions';
import { Category, SearchFilters } from '@app/types/store/thingCategories';

const remove = createReqAC<number, number, Error>(actions.THINGS_CATEGORIES.REMOVE);
const search = createReqAC<SearchFilters, Category[], Error>(actions.THINGS_CATEGORIES.SEARCH);
const save = createReqAC<Category, Category, Error>(actions.THINGS_CATEGORIES.SAVE);

export default { search, save, remove };
