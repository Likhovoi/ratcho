import ACTIONS from '@app/store/actions';
import { initialReqState, nextReqState, switchAction } from '@app/ext/redux/boilerplate';
import { merge } from 'lodash';
import { ReqAction } from '@app/types/redux/boilerplate';
import { ReducerState, Category } from '@app/types/store/thingCategories';
import { findIndex } from 'lodash';

const initialState = {
  search: initialReqState(),
  save: initialReqState(),
  remove: initialReqState(),
  categories: null,
};

export default function (state: ReducerState = initialState, action: ReqAction<any>) {
  let newState = merge({}, state, {
    search: nextReqState(state.search, action, ACTIONS.THINGS_CATEGORIES.SEARCH),
    save: nextReqState(state.save, action, ACTIONS.THINGS_CATEGORIES.SAVE),
    remove: nextReqState(state.remove, action, ACTIONS.THINGS_CATEGORIES.REMOVE),
  });

  newState = switchAction(newState, action, {
    [ACTIONS.THINGS_CATEGORIES.SEARCH.RECEIVED]: (a: ReqAction<Category[]>) => {
      newState.categories = a.data;
    },
    [ACTIONS.THINGS_CATEGORIES.SAVE.RECEIVED]: (a: ReqAction<Category>) => {
      if (newState.categories) {
        const index = findIndex(newState.categories, item => item.id === a.data.id);
        if (index > -1) {
          newState.categories[index] = action.data;
        }
      }
    },
    [ACTIONS.THINGS_CATEGORIES.REMOVE.RECEIVED]: (a: ReqAction<number>) => {
      if (newState.categories) {
        newState.categories = newState.categories.filter(item => item.id !== a.data);
      }
    },
  });

  return newState;
}
