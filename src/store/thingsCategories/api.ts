import { http } from '@app/services';
import { send } from '@app/ext/axios/api';
import { SearchFilters, Category } from '@app/types/store/thingCategories';

export async function search(params: SearchFilters): Promise<Category[]> {
  return send(http.get<Category[]>('things-categories/search', { params }));
}

export async function remove(id: number): Promise<void> {
  return send(http.post<void>('things-categories/remove/' + id));
}

export async function save(data: Category): Promise<Category> {
  if (data.id) {
    return send(http.post<Category>(`things-categories/update/${data.id}`, data));
  } else {
    return send(http.post<Category>('things-categories/create', data));
  }
}
