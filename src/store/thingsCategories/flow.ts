import { call, take, fork, put } from 'redux-saga/effects';
import ACTIONS from '@app/store/actions';
import { ReqAction } from '@app/types/redux/boilerplate';
import { SearchFilters, Category } from '@app/types/store/thingCategories';
import ac from '@app/store/thingsCategories/ac';
import { search, save, remove } from '@app/store/thingsCategories/api';

function* searchFlow() {
  while (true) {
    const action: ReqAction<SearchFilters> = yield take(ACTIONS.THINGS_CATEGORIES.SEARCH.REQUEST);
    try {
      const response: Category[] = yield call(search, action.data);
      yield put(ac.search.received(response));
    } catch (error) {
      yield put(ac.search.rejected(error));
    }
  }
}

function* saveFlow() {
  while (true) {
    const action: ReqAction<Category> = yield take(ACTIONS.THINGS_CATEGORIES.SAVE.REQUEST);
    try {
      const response: Category = yield call(save, action.data);
      yield put(ac.save.received(response));
    } catch (error) {
      yield put(ac.save.rejected(error));
    }
  }
}

function* removeFlow() {
  while (true) {
    const action: ReqAction<number> = yield take(ACTIONS.THINGS_CATEGORIES.REMOVE.REQUEST);
    try {
      yield call(remove, action.data);
      yield put(ac.remove.received(action.data));
    } catch (error) {
      yield put(ac.remove.rejected(error));
    }
  }
}

export default function* () {
  yield fork(searchFlow);
  yield fork(saveFlow);
  yield fork(removeFlow);
}
