import { createReqAC } from '@app/ext/redux/boilerplate';
import actions from '@app/store/actions';
import { SignInRequest, SignInResponse, SignUpRequest, SignUpResponse } from '@app/types/store/auth';

const signIn = createReqAC<SignInRequest, SignInResponse, Error>(actions.AUTH.SIGN_IN);
const signUp = createReqAC<SignUpRequest, SignUpResponse, Error>(actions.AUTH.SIGN_UP);
const signOut = () => ({ type: actions.AUTH.SIGN_OUT });

export default { signIn, signUp, signOut };
