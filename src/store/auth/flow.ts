import { call, take, fork, put } from 'redux-saga/effects';
import ACTIONS from '@app/store/actions';
import { signIn, signUp } from '@app/store/auth/api';
import authAC from '@app/store/auth/ac';
import { SignInRequest, SignInResponse, SignUpRequest, SignUpResponse } from '@app/types/store/auth';
import { ReqAction } from '@app/types/redux/boilerplate';

function* signInFlow() {
  while (true) {
    const action: ReqAction<SignInRequest> = yield take(ACTIONS.AUTH.SIGN_IN.REQUEST);
    try {
      const response: SignInResponse = yield call(signIn, action.data);
      yield put(authAC.signIn.received(response));
    } catch (error) {
      yield put(authAC.signIn.rejected(error));
    }
  }
}

function* signUpFlow() {
  while (true) {
    const action: ReqAction<SignUpRequest> = yield take(ACTIONS.AUTH.SIGN_UP.REQUEST);
    try {
      const response: SignUpResponse = yield call(signUp, action.data);
      yield put(authAC.signUp.received(response));
    } catch (error) {
      yield put(authAC.signUp.rejected(error));
    }
  }
}

export default function* () {
  yield fork(signInFlow);
  yield fork(signUpFlow);
}
