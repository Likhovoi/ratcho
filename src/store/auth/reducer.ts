import ACTIONS from '@app/store/actions';
import { initialReqState, nextReqState, switchAction } from '@app/ext/redux/boilerplate';
import { merge } from 'lodash';
import { AnyAction } from 'redux';
import { AuthState, SignInResponse } from '@app/types/store/auth';
import { ReqAction } from '@app/types/redux/boilerplate';

const initialState = {
  signUp: initialReqState(),
  signIn: initialReqState(),
  token: localStorage.getItem('authToken') || '',
};

export default function (state: AuthState = initialState, action: ReqAction<any>) {
  let newState: AuthState = merge({}, state, {
    signUp: nextReqState(state.signUp, action, ACTIONS.AUTH.SIGN_UP),
    signIn: nextReqState(state.signUp, action, ACTIONS.AUTH.SIGN_IN),
  });
  newState = switchAction(newState, action, {
    [ACTIONS.AUTH.SIGN_OUT]: (a: ReqAction<void>) => ({
      token: '',
    }),
    [ACTIONS.AUTH.SIGN_IN.RECEIVED]: (a: ReqAction<SignInResponse>) => ({
      token: a.data.api_token,
    })
  });
  localStorage.setItem('authToken', newState.token);
  return newState;
}
