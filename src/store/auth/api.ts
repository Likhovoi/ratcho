import { http } from '@app/services';
import { send } from '@app/ext/axios/api';
import { SignInRequest, SignInResponse, SignUpRequest, SignUpResponse } from '@app/types/store/auth';

export async function signIn(data: SignInRequest): Promise<SignInResponse> {
  return send(http.post<SignInResponse>('sign-in', data));
}

export async function signUp(data: SignUpRequest): Promise<SignUpResponse> {
  return send(http.post<SignUpResponse>('sign-up', data));
}
