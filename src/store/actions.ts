import { createReqActionTypes } from '@app/ext/redux/boilerplate';

export default {
  AUTH: {
    SIGN_UP: createReqActionTypes('AUTH_SIGN_UP'),
    SIGN_IN: createReqActionTypes('AUTH_SIGN_IN'),
    SIGN_OUT: 'AUTH_SIGN_OUT',
  },
  USER: {
    CLEAR: 'USER_CLEAR',
    ACCOUNT_LOAD: createReqActionTypes('USER_ACCOUNT_LOAD'),
  },
  THINGS_CATEGORIES: {
    SEARCH: createReqActionTypes('THINGS_CATEGORIES_SEARCH'),
    SAVE: createReqActionTypes('THINGS_CATEGORIES_SAVE'),
    REMOVE: createReqActionTypes('THINGS_CATEGORIES_REMOVE'),
  }
};
