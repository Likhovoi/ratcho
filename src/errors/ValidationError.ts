import { InvalidationMessages } from '@app/types';

export class ValidationError extends Error {
  private messages: InvalidationMessages;

  constructor(messages: InvalidationMessages) {
      super('Invalid data error');
      Object.setPrototypeOf(this, ValidationError.prototype);
      this.messages = messages;
  }

  public getMessages() {
    return this.messages;
  }
}
