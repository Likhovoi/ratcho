import { InvalidationMessages } from '@app/types';
import { FormErrors } from 'redux-form';

export function dotsToBrackets<FormData>(messages: InvalidationMessages): FormErrors<FormData> {
  const formFails: FormErrors<FormData> = {};
  Object.keys(messages).forEach(dotKey => {
    const formKey = dotKey.replace(/\.(\d+)(\.)?/gi, '[$1]$2');
    formFails[formKey] = messages[dotKey];
  });
  return formFails;
}
