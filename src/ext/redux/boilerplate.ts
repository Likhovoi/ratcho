import { clone, merge } from 'lodash';
import { ReqAC, ReqActionTypes, ReqState } from '@app/types/redux/boilerplate.d';
import { AnyAction } from 'redux';

export function nextReqState(state: ReqState, action: AnyAction, reqActionTypes: ReqActionTypes): ReqState {
  if (reqActionTypes.RESET === action.type) {
    return initialReqState();
  } else if (reqActionTypes.REQUEST === action.type) {
    return {
      request: true,
      received: state.received,
      rejected: state.rejected,
      error: state.error,
    };
  } else if (reqActionTypes.RECEIVED === action.type) {
    return {
      request: false,
      received: true,
      rejected: false,
      error: state.error,
    };
  } else if (reqActionTypes.REJECTED === action.type) {
    return {
      request: false,
      received: false,
      rejected: true,
      error: action.data,
    };
  } else {
    return clone(state);
  }
}

export function switchAction<T>(state: T, action: AnyAction, switcher: object): T {
  if (switcher[action.type]) {
    const res = switcher[action.type](action);
    if (res) {
      state = merge(state, res);
    }
  }
  return state;
}

export function createReqActionTypes(prefix: string): ReqActionTypes {
  return {
    REQUEST: `${prefix}_REQUEST`,
    RECEIVED: `${prefix}_RECEIVED`,
    REJECTED: `${prefix}_REJECTED`,
    RESET: `${prefix}_RESET`,
  };
}

export function initialReqState(): ReqState {
  return {
    request: false,
    received: false,
    rejected: false,
    error: null,
  };
}

export function createReqAC<Req, Rec, Rej>(actionTypes: ReqActionTypes): ReqAC<Req, Rec, Rej> {
  return {
    request(data: Req) {
      return {
        type: actionTypes.REQUEST,
        data,
      };
    },
    received(data: Rec) {
      return {
        type: actionTypes.RECEIVED,
        data,
      };
    },
    rejected(data: Rej) {
      return {
        type: actionTypes.REJECTED,
        data,
      };
    },
    reset() {
      return {
        type: actionTypes.RESET,
      };
    }
  };
}
