import { ValidationError } from '@app/errors/ValidationError';
import { ServerError } from '@app/errors/ServerError';
import { AxiosError, AxiosPromise, AxiosResponse } from 'axios';
import { InvalidationMessages } from '@app/types';

export async function send<Res>(promise: AxiosPromise<Res>): Promise<Res> {
  try {
    const response = await promise;
    return response.data;
  } catch (error) {
    const ex = error as AxiosError;
    const response = ex.response as AxiosResponse<InvalidationMessages | string>;
    if (response) {
      if (response.status === 422) {
        throw new ValidationError(response.data as InvalidationMessages);
      } else {
        throw new ServerError(response.data as string);
      }
    } else {
      throw ex;
    }
  }
}
