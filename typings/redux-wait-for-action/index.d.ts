declare module 'redux-wait-for-action' {
  import { Middleware, Action } from 'redux';

  export default function reduxWaitForActionMiddleware(): Middleware;
  export const WAIT_FOR_ACTION: string;
  export const ERROR_ACTION: string;
  export interface Dispatch<S, Res> {
    <A extends Action>(action: A): Promise<Res>;
  }
}
